Cutadapt GeneFlow App
=====================

Version: 2.1-02

This GeneFlow2 app wraps the Cutadapt tool.

Inputs
------

1. input: Sequence FASTQ File

2. pair: Sequence FASTQ File Pair

Parameters
----------

1. adaptor_b: Cutadapt 5' or 3' adaptor to be removed. Default: ATGC

2. quality_cutoff: Quality cutoff. Default: 25

3. error_rate: Error rate. Default: 0.3

4. minimum_length: Minimum length. Default: 75

5. output: Output directory.
